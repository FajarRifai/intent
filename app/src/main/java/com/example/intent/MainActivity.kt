package com.example.intent

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Explicit
        val btnIntent = findViewById<Button>(R.id.secondActivity)
        btnIntent.setOnClickListener(this)

        val btnSend = findViewById<Button>(R.id.btnSend)
        btnSend.setOnClickListener {
            sendMessage()
        }

        //implicit dial number
        val edPhone = findViewById<EditText>(R.id.edPhone)
        val btnPhone = findViewById<Button>(R.id.btnPhone)
        btnPhone.setOnClickListener {
            val intent =  Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+edPhone.text.toString()))
            startActivity(intent)
        }

    }

    //Explicit Without Data
    override fun onClick(v: View) {
        when(v.id){
            R.id.secondActivity ->{
                val showIntent = Intent(this@MainActivity, SecondActivity::class.java)
                startActivity(showIntent)
            }
        }
    }
    //Explicit With Data
    private fun sendMessage() {
        val edtInput = findViewById<EditText>(R.id.edtInput)
        val message = edtInput.text.toString()
        val intent = Intent(this,SecondActivityWithData::class.java)
        intent.apply {
            putExtra("theMessage", message)
        }
        startActivity(intent)
    }
}